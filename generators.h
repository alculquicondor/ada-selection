#ifndef _GENERATORS_H
#define _GENERATORS_H

#include <vector>
#include <cstdlib>
#include <ctime>

std::vector<int> GenerateOrdered(size_t sz) {
  std::vector<int> V(sz);
  for (size_t i = 0; i < sz; ++i)
    V[i] = i + 1;
  return V;
}

template<typename Iterator>
void KnuthShuffle(Iterator begin, Iterator end) {
  srand(time(NULL));
  for ( ; begin != end; ++begin)
    std::swap(*begin, *(begin + rand() % (end-begin)));
}

template<typename Iterator>
void KnuthShuffleProbabilistic(Iterator begin, Iterator end, size_t k) {
  srand(time(NULL));
  double probability = k / double(end-begin);
  for ( ; begin != end; ++begin)
    if (rand() / double(RAND_MAX) < probability)
      std::swap(*begin, *(begin + rand() % (end-begin)));
}


#endif /* end of include guard: _GENERATORS_H */

