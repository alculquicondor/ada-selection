CC = g++ -Wall --std=c++11

main: main.cc selection.h
	$(CC) -O2 main.cc -o main

run_test: test
	./test

test: test.cc selection_test.h selection.h
	$(CC) -g test.cc -o test

clean:
	rm -f main test
