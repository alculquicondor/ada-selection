#ifndef _SELECTION_H
#define _SELECTION_H

#include <cstdlib>
#include <ctime>
#include <algorithm>

template<typename Iterator>
class TraverseSelection {
  typedef typename Iterator::value_type value_type;
 public:
  value_type operator()(Iterator begin, Iterator end, size_t k) {
    ++k;
    while (k--) {
      Iterator min_it = begin;
      for (Iterator j = begin + 1; j != end; ++j)
        if (*j < *min_it)
          min_it = j;
      std::swap(*begin++, *min_it);
    }
    return *(begin-1);
  }
};

template<typename Iterator>
class HeapSelection {
  typedef typename Iterator::value_type value_type;
  static inline size_t LeftChild(size_t id) {
    return (id << 1) + 1;
  }
  static inline size_t RightChild(size_t id) {
    return (id << 1) + 2;
  }
  void HeapifyDown(Iterator begin, size_t sz, size_t id) {
    typename Iterator::value_type pivot = *(begin+id);
    size_t next = LeftChild(id), tmp;
    while (next < sz) {
      if ((tmp = RightChild(id)) < sz and *(begin+tmp) < *(begin+next))
        next = tmp;
      if (*(begin+next) < pivot) {
        *(begin+id) = *(begin+next);
        id = next;
        next = LeftChild(id);
      } else {
        next = sz;
      }
    }
    *(begin+id) = pivot;
  }
 public:
  value_type operator()(Iterator begin, Iterator end, size_t k) {
    size_t sz = end - begin;
    for (Iterator i = begin + (sz >> 1); i >= begin; --i)
      HeapifyDown(begin, sz, i - begin);
    for (size_t i = 0; i <= k; ++i) {
      std::swap(*begin, *(begin+--sz));
      HeapifyDown(begin, sz, 0);
    }
    return *(begin+sz);
  }
};

template<typename Iterator>
class RandomizedSelection {
  typedef typename Iterator::value_type value_type;
  Iterator RandomizedPartition(Iterator begin, Iterator end) {
    std::swap(*(begin + rand() % (end - begin)), *begin);
    Iterator i = begin;
    for (Iterator j = begin + 1; j != end; ++j)
      if (*j < *begin)
        std::swap(*j, *(++i));
    std::swap(*begin, *i);
    return i;
  }
 public:
  value_type operator()(Iterator begin, Iterator end, size_t k) {
    srand(time(nullptr));
    Iterator p;
    while (begin != end) {
      p = RandomizedPartition(begin, end);
      if (p == begin + k) {
        return *p;
      } else if (begin + k < p) {
        end = p;
      } else {
        k = begin + k - p - 1;
        begin = p + 1;
      }
    }
    return *begin;
  }
};

template<typename Iterator>
class DeterministicSelection {
  typedef typename Iterator::value_type value_type;
 public:
  value_type operator()(Iterator begin, Iterator end, size_t k);

 private:
  void InsertionSort(Iterator begin, Iterator end) {
    for (Iterator i = begin + 1; i != end; ++i) {
      Iterator j;
      value_type current = *i;
      for (j = i - 1; current < *j and j >= begin; --j)
        *(j + 1) = *j;
      *(j + 1) = current;
    }
  }
  Iterator DeterministicPartition(Iterator begin, Iterator end) {
    std::vector<value_type> medians;
    medians.reserve((end - begin + 4) / 5);
    for (Iterator i = begin; i < end; i += 5) {
      InsertionSort(i, std::min(i + 5, end));
      medians.push_back(*std::min(i+2, end-1));
    }
    value_type median_of_medians =
      (*this)(medians.begin(), medians.end(), medians.size() >> 1);
    std::swap(*begin, *find(begin, end, median_of_medians));
    Iterator i = begin;
    for (Iterator j = begin + 1; j != end; ++j)
      if (*j < *begin)
        std::swap(*j, *(++i));
    std::swap(*begin, *i);
    return i;
  }
};

template<typename Iterator>
typename DeterministicSelection<Iterator>::
value_type DeterministicSelection<Iterator>::operator()(
        Iterator begin, Iterator end, size_t k) {
  Iterator p;
  while (end - begin > 5) {
    p = DeterministicPartition(begin, end);
    if (p == begin + k) {
      return *p;
    } else if (begin + k < p) {
      end = p;
    } else {
      k = begin + k - p - 1;
      begin = p + 1;
    }
  }
  InsertionSort(begin, end);
  return *(begin+k);
}

#endif /* end of include guard: _SELECTION_H */

