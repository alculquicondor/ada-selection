#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <cmath>
#include <typeinfo>

#include "selection.h"
#include "generators.h"

using namespace std;

template<typename Vector, typename Func>
void RunInstance(const Vector& data, Func f, size_t k, size_t rep = 3, bool check = false) {
  cout << " & ";
  if (check and data.size() * k > 10000000000LL) {
    cout << "--";
    return;
  }
  size_t avgTime = 0;
  for (size_t j = 0; j < rep; ++j) {
    Vector data_(data);
    chrono::time_point<std::chrono::system_clock> start, end;
    start = chrono::system_clock::now();
    typename Vector::value_type ans = f(data_.begin(), data_.end(), k);
    end = chrono::system_clock::now();
    avgTime += chrono::duration_cast<chrono::microseconds>(end-start).count();
  }
  cout << '$' << avgTime / (rep * 1000.) << '$';
}

typedef vector<int>::iterator VIterator;

void Runs(const vector<int> &V) {
  vector<string> label{"5", "\\lg{n}", "\\sqrt{n}", "n/2"};
  double n = V.size();
  vector<size_t> sz{5, size_t(log2(n)), size_t(sqrt(n)), size_t(n / 2)};
  for (int i = 0; i < 4; ++i) {
    cout << "& $" << label[i] << "$";
    RunInstance(V, TraverseSelection<VIterator>(), sz[i], 3, true);
    RunInstance(V, HeapSelection<VIterator>(), sz[i]);
    RunInstance(V, RandomizedSelection<VIterator>(), sz[i], 10);
    RunInstance(V, DeterministicSelection<VIterator>(), sz[i]);
    cout << "\\\\\n";
  }
  cout << "\\hline\n" << endl;
}

int main() {
  std::ios::sync_with_stdio(false);
  cout << fixed << setprecision(3);
  for (int i = 0, n = 1000; i < 16; ++i, n <<= 1) {
    vector<int> V = GenerateOrdered(n);
    KnuthShuffle(V.begin(), V.end());
    cout << "\\multirow{4}{*}{$2^{" << i << "}1000$}\n";
    Runs(V);
  }
  cout << "*************************\n\n";
  for (int k = 1; k <= 20; ++k) {
    cout << "\\multirow{4}{*}{$\\frac{2^{"<< k <<"}}{2^{20}}$}\n";
    vector<int> V = GenerateOrdered(1<<20);
    KnuthShuffleProbabilistic(V.begin(), V.end(), 1 << k);
    Runs(V);
  }
  return 0;
}

