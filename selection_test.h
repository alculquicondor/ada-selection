#ifndef _SELECTION_TEST_H
#define _SELECTION_TEST_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <typeinfo>
#include <cassert>
#include "selection.h"

template<typename Func>
void TestSelection(Func f) {
  std::vector<int> data{3, 4, 21, 11, 14, 8, 1, 12, 19, 9, 10, 15, 2, 7, 23, 16, 6, 20, 5, 18, 17, 13, 22};
  std::cerr << std::setfill('0');
  std::cerr << "Testing " << typeid(f).name() << ":\n";
  for (size_t i = 0; i < data.size(); ++i) {
    std::cerr << "  Test " << std::setw(2) << i + 1;
    std::vector<int> c = data;
    assert(f(c.begin(), c.end(), i) == (int)i + 1);
    std::cerr << " OK\n";
  }
  std::cerr << "===============\n";
}

void RunTests() {
  TestSelection(TraverseSelection<std::vector<int>::iterator>());
  TestSelection(HeapSelection<std::vector<int>::iterator>());
  TestSelection(RandomizedSelection<std::vector<int>::iterator>());
  TestSelection(DeterministicSelection<std::vector<int>::iterator>());
}


#endif /* end of include guard: _SELECTION_TEST_H */

